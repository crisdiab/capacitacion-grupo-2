import { PermisosGuard } from './../permisos.guard';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioService } from './usuario.service';
import { Controller, Delete, Get, Post, Put, Request, UseGuards } from '@nestjs/common';
import { Param } from '@nestjs/common';
import { Body } from '@nestjs/common';
import { PermisosDecorador } from './../permisos.decorador';

@Controller('usuario') // path principal http://localhost:3000/usuario
export class UsuarioController {

    constructor(
        private readonly usuarioService: UsuarioService
    ){}


    @UseGuards(AuthGuard('jwt'), PermisosGuard)
    @Get('obtener-usuarios') // http://localhost:3000/usuario/obtener-usuarios
    @PermisosDecorador('listar:usuario')
    obtenerUsuarios(@Request() request) {
        const usuarios = this.usuarioService.listarUsuarios();
        return {
            mensaje: 'Todos los usuarios',
            data: usuarios,
        }
    }

    @Post('crear-usuario')
    crearUsuario(@Body() usuarioACrear) {
       console.log('usuario a crear en body', usuarioACrear)
        const usuarioCreado = this.usuarioService.crearUsuario(usuarioACrear);
        return {
            mensaje: 'usuario creado',
            data: usuarioCreado
        }
    }

    @Delete('eliminar-usuario/:id') // http://localhost:3000/usuario/eliminar-usuario
    eliminarUsuarios(@Param('id') id: string) {
       const usuarioEliminado = this.usuarioService.eliminarUsuario(id);
        return {
            mensaje: 'usuario eliminado',
            data: usuarioEliminado
        }
    }



    @Put('editar-usuario/:id')
    editarUnUsuario(@Param('id')id: string,
    @Body() usuarioAActualizar
    ) {
      const usuarioActualizado =  this.usuarioService.actualizar(id, usuarioAActualizar)
        return {
            mensaje: 'usuario editado',
            data: usuarioActualizado
        }

    }

    @Get('obtener-por-id')
    filtrarPorId(@Param('id') idParametro: string){
        const usuarioFiltrado = this.usuarioService
        .filtrarPorId(idParametro)
        return {
            mensaje: 'filtrado por id',
            data: usuarioFiltrado
        }
    }
}
