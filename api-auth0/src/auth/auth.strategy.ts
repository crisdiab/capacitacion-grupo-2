import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { passportJwtSecret } from 'jwks-rsa';
import * as dotenv from 'dotenv';

dotenv.config();

export class AuthStrategy extends PassportStrategy(Strategy) {

    constructor(){
        console.log('variable de entorno', process.env.AUTH0_DOMINIO)
        const configuracionStrategy = {
            secretOrKeyProvider: passportJwtSecret({
                cache: true,
                rateLimit: true,
                jwksRequestsPerMinute: 5,
                jwksUri:`${process.env.AUTH0_DOMINIO}.well-known/jwks.json`
              //  jwksUri: process.env.AUTH0_DOMINIO + '.well-known/jwks.json'
            }),
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            audience: process.env.AUTH0_AUDIENCE, // clientId
            issuer:  process.env.AUTH0_DOMINIO, // dominio
            algorithm: ['RS256']
        }
        super(
          configuracionStrategy  
        )
    }

validate(payload:any){
    return payload;
}
}