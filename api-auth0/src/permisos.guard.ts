import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class PermisosGuard implements CanActivate {


  constructor(private reflector: Reflector){}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    
const permisosRuta = this.reflector.get<string[]>
('permissions', context.getHandler())

const permisosUsuario = context.getArgs()[0].user.permissions;
if(!permisosRuta){
  return true;
}
permisosUsuario.push('listar:usuario')
console.log('permisos usuario', permisosUsuario)
const tienePermisos = () => 
permisosRuta.every(
  rutaPermiso => permisosUsuario.includes(rutaPermiso)
)

return tienePermisos();
  }
}
