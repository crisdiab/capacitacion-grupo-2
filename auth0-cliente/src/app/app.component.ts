import { Component, OnInit } from '@angular/core';
import { AuthService, User } from '@auth0/auth0-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'auth0-cliente';
  estaLogeado = false;
usuario: User;
constructor(private readonly auth0Service: AuthService) {}

ngOnInit(){
  this.verificarUsuarioLogeado();
  this.recuperarInfoUsuario()
}

  login(){
    this.auth0Service.loginWithRedirect();
  }

  logout(){
    this.auth0Service.logout();
  }

  verificarUsuarioLogeado(){
    this.auth0Service.isAuthenticated$
    .subscribe(
      respuesta => this.estaLogeado = respuesta
    )
  }

  recuperarInfoUsuario(){
    this.auth0Service.user$
    .subscribe(
      respuesta => this.usuario = respuesta
    )
  }
}
