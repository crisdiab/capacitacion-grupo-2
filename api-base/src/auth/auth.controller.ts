import { CredencialesInterface } from './credenciales.interface';
import { AuthService } from './auth.service';
import { BadRequestException, Body, Controller, Post } from '@nestjs/common';

@Controller('auth')
export class AuthController {

constructor(private readonly authService:AuthService){}

    @Post('login') // http://localhost:3000/auth/login
    login(@Body() credenciales: CredencialesInterface){

        const existenCredenciales = credenciales.usuario && credenciales.password
        if(existenCredenciales){
            const tokenGenerado = this.authService
            .loginConCredenciales(credenciales);
            return {
                access_token: tokenGenerado,
            }
        }else {
            const mensajeError = {
                code: 400,
                mensaje: 'No envia credenciales'
            }
            throw new BadRequestException(mensajeError)
        }
    }
}
