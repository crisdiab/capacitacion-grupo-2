import { CredencialesInterface } from './credenciales.interface';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {

    constructor(private readonly jwtService: JwtService){}

    loginConCredenciales(credenciales: CredencialesInterface){
// agregar la logica de buscar el usuario si existe en el sistema y si las credenciales son correctas
// payload
const payload = {
    email: credenciales.usuario,
    clave: credenciales.password
};
        const token = this.jwtService.sign(payload)
        return token
    }
}
